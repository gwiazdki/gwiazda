﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gwiazda.Models
{
    public class CardForm
    {               
        public string Number { get; set; }
        public int Year { get; set; }
        public string WasteCode { get; set; }
        public string WasteKind { get; set; }
        public string Pcb { get; set; }
        public ICollection<SubCardForm> SubCardForms { get; set; }


        public System.Guid KpoDto()
        {
            Id = Guid.NewGuid();
            return Id;
        }
        
        #region Model dokumentu KPO w mongo
        /// <summary>
        /// Identyfikator nadawany automatycznie.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Kod odpadu.
        /// </summary>
        public string WasteId { get; set; }

        /// <summary>
        /// Rodzaj odpadu.
        /// </summary>
        public string WasteCategory { get; set; }

        /// <summary>
        /// Rodzaj odpadu.
        /// </summary>
        //[BsonElement("wastetype")]
        //public string WasteType { get; set; }

        /// <summary>
        /// Waga odpadu [Mg].
        /// </summary>
        //[BsonRequired]
        public double? WasteWeight { get; set; }

        /// <summary>
        /// Flaga do oznaczania odpadów niebezpieczncyh dla środowiska.
        /// </summary>
        public bool? WasteBiohazard { get; set; }

        /// <summary>
        /// Nr certyfikatu.
        /// </summary>
        public string WasteCertNo { get; set; }

        /// <summary>
        /// Nazwa jednostki lub firmy przekazującej odpad.
        /// </summary>
        public string SenderName { get; set; }

        /// <summary>
        /// Adres jednostki lub firmy przekazującej odpad.
        /// </summary>
        public string SenderAddress { get; set; }

        /// <summary>
        /// REGON jednostki lub firmy przekazującej odpad.
        /// </summary>
        public string SenderREGON { get; set; }

        /// <summary>
        /// NIP jednostki lub firmy przekazującej odpad.
        /// </summary>
        public string SenderNIP { get; set; }

        /// <summary>
        /// Miejsce prowadzenia działalności jednostki lub firmy przekazującej odpad.
        /// </summary>
        public string[] SenderHQ { get; set; }

        /// <summary>
        /// Data przekazania odpadu.
        /// </summary>
        public DateTime? SenderOperationDate { get; set; }

        /// <summary>
        /// Nazwa jednostki lub firmy transportującej odpad.
        /// </summary>
        public string TransportName { get; set; }

        /// <summary>
        /// Adres jednostki lub firmy transportującej odpad.
        /// </summary>
        public string TransportAddress { get; set; }

        /// <summary>
        /// REGON jednostki lub firmy transportującej odpad.
        /// </summary>
        public string TransportREGON { get; set; }

        /// <summary>
        /// NIP jednostki lub firmy transportującej odpad.
        /// </summary>
        public string TransportNIP { get; set; }

        /// <summary>
        /// Nr rejestracyjny pojazdu, przyczepy lub naczepy.
        /// </summary>
        public string TransportRegNo { get; set; }

        /// <summary>
        /// Data transportowania odpadu.
        /// </summary>
        public DateTime? TransportOperationDate { get; set; }

        /// <summary>
        /// Nazwa jednostki lub firmy przejmującej odpad.
        /// </summary>
        public string ReceiverName { get; set; }

        /// <summary>
        /// Adres jednostki lub firmy przejmującej odpad.
        /// </summary>
        public string ReceiverAddress { get; set; }

        /// <summary>
        /// REGON jednostki lub firmy przejmującej odpad.
        /// </summary>
        public string ReceiverREGON { get; set; }

        /// <summary>
        /// NIP jednostki lub firmy przejmującej odpad.
        /// </summary>
        public string ReceiverNIP { get; set; }

        /// <summary>
        /// Miejsce prowadzenia działalności jednostki lub firmy przejmującej odpad.
        /// </summary>
        public string[] ReceiverHQ { get; set; }

        /// <summary>
        /// Data przejęcia odpadu.
        /// </summary>
        public DateTime? ReceiverOperationDate { get; set; }

        #endregion

        #region Parametry kontrolne dokumentu

        /// <summary>
        /// Data wytworzenia dokumentu.
        /// </summary>
        //[BsonElement("datecreated")]
        //public DateTime DateCreated { get; set; }

        public DateTime? OperationDate { get; set; }

        ///// <summary>
        ///// Data wprowadzenia dokumentu do bazy.
        ///// </summary>
        //[BsonRequired]
        //[BsonElement("dateinserted")]
        //public DateTime DateInserted { get; set; }

        ///// <summary>
        ///// Data edycji dokumentu.
        ///// </summary>
        //[BsonElement("datelastedit")]
        //public DateTime? DateLastEdit { get; set; }

        ///// <summary>
        ///// Login osoby edytującej.
        ///// </summary>
        //[BsonElement("editorlogin")]
        //public string EditorLogin { get; set; }

        ///// <summary>
        ///// Nazwa osoby lub podmiotu edytującego.
        ///// </summary>
        //[BsonElement("editorsname")]
        //public string EditorName { get; set; }

        ///// <summary>
        ///// Flaga określająca status aktywności dokumentu KPO.
        ///// </summary>
        //[BsonElement("isactive")]
        //public bool IsActive { get; set; }

        ///// <summary>
        ///// Data deaktywacji dokumentu KPO.
        ///// </summary>
        //[BsonElement("dateinactive")]
        //public DateTime? DateInactive { get; set; }

        #endregion
 
    }
}
