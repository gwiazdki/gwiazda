﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Gwiazda.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Gwiazda.Controllers
{
    public class CardFormController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {            
            return View(new CardForm());
        }

        public IActionResult Edit()
        {
            CardForm Object = new CardForm {
                Number = "1",
                Year = 2018,
                WasteCode = "2",
                WasteKind = "3",
                Pcb = "4",
                SubCardForms = new List<SubCardForm>{ new SubCardForm {Number = "5"}, new SubCardForm { Number = "3" } } };            

            return View(Object);
        }
    }
}
