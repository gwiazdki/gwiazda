﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Gwiazda.Models;

namespace Gwiazda.Controllers
{
    public class WasteTransferCardController : Controller
    {
        // GET: WasteTransferCard
        public ActionResult Index()
        {
            return View();
        }

        // GET: WasteTransferCard/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: WasteTransferCard/Create
        public ActionResult Create()
        {
            return View();
        }

        // // POST: WasteTransferCard/Create
        // [HttpPost]
        // [ValidateAntiForgeryToken]
        // public ActionResult Create(IFormCollection collection)
        // {
        //     try
        //     {
        //         // TODO: Add insert logic here

        //         return RedirectToAction(nameof(Index));
        //     }
        //     catch
        //     {
        //         return View();
        //     }
        // }

        // GET: WasteTransferCard/Edit/5
        public ActionResult Edit()
        {
            // return View();

                WasteTransferCard Object = new WasteTransferCard {
                Number = "1",
                Year = 2018,
                WasteCode = "2",
                WasteKind = "3",
                Pcb = "4",
                SubCardForms = new List<SubCardForm>{ new SubCardForm {Number = "5"}, new SubCardForm { Number = "3" } } 
                };            

            return View(Object);


            
        }

        // // POST: WasteTransferCard/Edit/5
        // [HttpPost]
        // [ValidateAntiForgeryToken]
        // public ActionResult Edit(int id, IFormCollection collection)
        // {
        //     try
        //     {
        //         // TODO: Add update logic here

        //         return RedirectToAction(nameof(Index));
        //     }
        //     catch
        //     {
        //         return View();
        //     }
        // }

        // GET: WasteTransferCard/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // // POST: WasteTransferCard/Delete/5
        // [HttpPost]
        // [ValidateAntiForgeryToken]
        // public ActionResult Delete(int id, IFormCollection collection)
        // {
        //     try
        //     {
        //         // TODO: Add delete logic here

        //         return RedirectToAction(nameof(Index));
        //     }
        //     catch
        //     {
        //         return View();
        //     }
        // }
    }
}